# Projet_IA



## Getting started

Dans ce Git vous trouverez un notebook, un rapport ainsi qu'un fichier csv contenant les données de l'étude.

Pour pouvoir executer le projet, il faut télécharger le notebook et le mettre dans son drive.
Afin qu'il soit fonctionnel, il faut télécharger le fichier "All_Participant_resultat_corrCollisions_delimiter_propositionFinale.csv" et egalement le mettre dans son drive.
Il faut ensuite récuperer l'ID du fichier et l'inserer à l'endroit nécessaire comme ci-dessous:

downloaded = drive.CreateFile({'id':'id_du_fichier'}) # replace the id with id of file you want to access
downloaded.GetContentFile('All_Participant_resultat_corrCollisions_delimiter_propositionFinale2.csv')

Pour récupérer l'ID du fichier il faut faire un clique droit dessus, aller sur "partager" puis "copier le lien"
On obtiens ainsi une url comme ci-dessous:

https://drive.google.com/file/d/1qrADDJ4ak-1NgdteCOwwiNO4smjUqNRV/view?usp=drive_link

où 1qrADDJ4ak-1NgdteCOwwiNO4smjUqNRV est l'ID

On a ainsi le template: https://drive.google.com/file/d/ID_du_fichier/view?usp=drive_link

Après cela le notebook est executable.